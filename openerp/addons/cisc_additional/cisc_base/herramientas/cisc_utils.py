
# -*- encoding: utf-8 -*-
########################################################################
#                                                                       
# @authors: Jairo Troncoso Migrated 2013                                                                           
# Copyright (C) 2013  CISC    
# @authors: Christopher Ormaza                                                                           
#                                                                                                       
#This program is free software: you can redistribute it and/or modify   
#it under the terms of the GNU General Public License as published by   
#the Free Software Foundation, either version 3 of the License, or      
#(at your option) any later version.                                    
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#                                                                       
#This program is distributed in the hope that it will be useful,        
#but WITHOUT ANY WARRANTY; without even the implied warranty of         
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
#GNU General Public License for more details.                           
#                                                                       
#You should have received a copy of the GNU General Public License      
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

from osv import osv
from osv import fields
import decimal_precision as dp
from tools.translate import _
import tools
import netsvc
import re
from datetime import datetime, timedelta

class cisc_utils(osv.osv):

    _name = 'cisc.utils'
    _description = 'Utilidades Varias'

    def create_report(self, cr, uid, ids, report, model=None, name=False, context=None):
        if not context:
            context = {}
        logger = netsvc.Logger()
        files = []
        p = re.compile('[/:()<>|?*]|(\\\)')
        id = ids
        try:
            result = None
            format = None
            service = netsvc.LocalService('report.' + report)
            if not context.get('active_id'):
                context['active_id'] = None
            context['active_model'] = model
            if id == 0:
                (result, format) = service.create(cr, uid, [], {'model':model,'ids':[]}, context=context)
            else:
                (result, format) = service.create(cr, uid, [id], {'model':model,'ids':[id]}, context=context)
            if not name:
                report_file = '/tmp/report_'+ str(id) + '.' + format
            else:
                name = p.sub('_', name) 
                report_file = name + '.' + format
            files += [(report_file, result)]    
        except Exception,e:
            logger.notifyChannel("Error creating Report", netsvc.LOG_WARNING, str(e))        
        return files

    def get_data_notification(self, cr, uid, module, xml_id, context=None):
        if not context:
            context = {}
        res = {
               'name': '',
               'header': '',
               'emails': [],
               'footer': ''
               }
        logger = netsvc.Logger()
        model_obj = self.pool.get('ir.model.data')
        not_obj = self.pool.get('util.notification')
        if not module or not xml_id:
            raise osv.except_osv(_('Programming Error'), _('You must set xml_id and module'))
        else:
            model_ids = model_obj.search(cr, uid, [('module','=',module), ('name','=',xml_id)])
            model = model_ids and model_obj.browse(cr, uid, model_ids[0]) or None
            notification = model and not_obj.browse(cr, uid, model.res_id) or None
            if notification:
                for user in notification.user_mails_ids:
                    if user.user_email:
                        res['emails'].append(user.user_email)
                    else:
                        logger.notifyChannel("warning", netsvc.LOG_WARNING,
                            _("User %s doesn't have email configured, you must assign email address to send notifications") % user.name)
                for group in notification.groups_mails_ids:
                    for user in group.users:
                        if user.user_email:
                            res['emails'].append(user.user_email)
                        else:
                            logger.notifyChannel("warning", netsvc.LOG_WARNING,
                                _("User %s doesn't have email configured, you must assign email address to send notifications") % user.name)
                if not res['emails']:
                    logger.notifyChannel("warning", netsvc.LOG_WARNING,
                        _('Notification "%s" doesn\'t have emails in users or groups assigned to send') % notification.name)
                res['header'] = notification.header or ''
                res['footer'] = notification.footer or ''
                res['name'] = notification.name or ''
            return res
        
    def send_notification_email(self, cr, uid, xml_id, module, 
                                content='', name_report='', model_report='', ids_report=[], 
                                name_file_report=None, cc_mails=[], context=None, send_from=None):
        mail_data = self.get_data_notification(cr, uid, module, xml_id, context)
        emails = []
        reports = []
        for mail in mail_data.get('emails'):
            emails.append(mail)
        for cc_mail in cc_mails:
            emails.append(cc_mail)
        if model_report:
            model_obj = self.pool.get(model_report)
            if ids_report and ids_report[0] == 0:
                reports.append(self.create_report(cr, uid, ids_report, name_report, model_report,  name_file_report, context)[0])
            else:
                for obj in model_obj.browse(cr, uid, ids_report):
                    name = name_file_report and (name_file_report + ' - ') or ''
                    name += model_obj.name_get(cr, uid, [obj.id])[0][1]
                    reports.append(self.create_report(cr, uid, ids_report, name_report, model_report,  name, context)[0])
        attachments = reports or []
        from_adr = tools.config.options['email_from']
        subject = mail_data.get('name')
        to_adr = emails
        header = mail_data.get('header') and (mail_data.get('header') + '\n') or '\n'
        footer = mail_data.get('footer') and (mail_data.get('footer') + '\n') or '\n'
        if not send_from:
            body = header + content + '\n' + footer + _(u'\n\n No responder este correo, ha sido creado automáticamente por OpenERP')
        else:
            from_adr = send_from
            body = header + content + '\n' + footer + _(u'Ha sido creado automáticamente por OpenERP')
        server_mail_obj = self.pool.get('ir.mail_server')
        message = server_mail_obj.build_email(from_adr, to_adr, subject, body, email_bcc=[from_adr], attachments=attachments, subtype='html')
        server_mail_obj.send_email(cr, uid, message, context=context)
        return True
    
    #http://stackoverflow.com/a/7029418
    def get_week_of_month(self, date):
        month = date.month
        week = 0
        while date.month == month:
            week += 1
            date -= timedelta(days=7)
        return week
    
    
    
cisc_utils()