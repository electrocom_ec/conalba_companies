# -*- encoding: utf-8 -*-
########################################################################
#
# @authors: Jairo Troncoso
# Copyright (C) 2014  CISC
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################
import netsvc
from osv import osv
from osv import fields
from tools.translate import _

class document_directory(osv.osv):
    _inherit = "document.directory"

    _columns={
              'not_unlink':fields.boolean('No eliminar', required=False),  
              }
    
    def unlink(self, cr, uid, ids, context=None):
         if not context:
             context={}
         for do_direc in self.browse(cr , uid , ids):
             if do_direc.not_unlink:
                 raise osv.except_osv(_('Error!'), _("No puedes eliminar este directorio" ))
         res = super(document_directory, self).unlink(cr, uid, ids, context)
         return res 
    
document_directory()