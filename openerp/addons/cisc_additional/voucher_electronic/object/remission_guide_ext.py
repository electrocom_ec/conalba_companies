# -*- encoding: utf-8 -*-
########################################################################
#
# @authors: Jairo Troncoso
# Copyright (C) 2014  CISC
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################
from osv import osv
from osv import fields
from tools.translate import _
import jasper_reports


def reportjasper_document( cr, uid, ids, data, context ):
    return {
        'parameters': {    
            'id': data['id'],
        }
   }


class remission_guide_ext(osv.osv):
    _inherit = 'remission.guide.ext'

    _columns={              
              'number':fields.char('Número comprobante', size=64, readonly=True),   
              'user_id':fields.many2one('res.users', 'Usuario', required=False),
              'company_id':fields.many2one('res.company', 'Compañia', required=False),          
              'partner_id':fields.many2one('res.partner', 'Cliente', readonly=True),
              'ruc_partner': fields.related('partner_id','vat', type='char', readonly=True, size=64, relation='res.partner', store=True, string='RUC / CI'),
              'remission_line_ext': fields.one2many('remission.guide.line.ext', 'remission_ext_id', 'Detalles', states={'authorized':[('readonly', True)],'key_contingency':[('readonly',True)]}),
              'note': fields.text('Otra información'), 
              'name_file':fields.char('nombre_archivo', size=64, readonly=True),
              'street_start':fields.char('Dirección de partida', size=200, states={'authorized':[('readonly', True)],'key_contingency':[('readonly',True)]}),
              'street_destination':fields.char('Direccion destinatario', size=200, states={'authorized':[('readonly', True)],'key_contingency':[('readonly',True)]}),
              'reason_social_carrier':fields.char('Razón social transportista', size=300, states={'authorized':[('readonly', True)],'key_contingency':[('readonly',True)]}),
              'ruc_carrier':fields.char('RUC /CI transportista', size=15, states={'authorized':[('readonly', True)],'key_contingency':[('readonly',True)]}),
              'date_start_transport':fields.date('Fecha inicio de transporte', size=32, states={'authorized':[('readonly', True)],'key_contingency':[('readonly',True)]}),
              'date_end_transport':fields.date('Fecha fin de transporte', size=32, states={'authorized':[('readonly', True)],'key_contingency':[('readonly',True)]}),
              'plate':fields.char('Placa', size=10, states={'authorized':[('readonly', True)],'key_contingency':[('readonly',True)]}),
              'type_ref_transp':fields.char('tipo identificacion transportista', size=2, readonly=True),
              'partner_name':fields.char('Cliente',size=128, states={'authorized':[('readonly', True)],'key_contingency':[('readonly',True)]}),
              'partner_email':fields.char('email',size=128, states={'authorized':[('readonly', True)],'key_contingency':[('readonly',True)]}),
              'motive_transfer':fields.char('Motivo de traslado', size=200, states={'authorized':[('readonly', True)],'key_contingency':[('readonly',True)]}),
              
              }
    
    _order = "number desc"
    _rec_name = 'number'
    
    _defaults = {
        'company_id': lambda self,cr,uid,c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.id,
        'state': 'loaded'
            }

    def document_electronic_remission_sri(self, cr, uid , ids , context=None):
        model = "remission.guide.ext"
        type_document = "guiaRemision"
        type_doc_dir = "Guias de Remisión"
        compr = "06"
        for remi in self.browse(cr , uid , ids , context=None): 
            self.pool.get("electronic.invoicing.sri").validation_document_sri(cr , uid, [remi.id], model, type_document, compr, type_doc_dir, context=None)
        return True 
    
    def imprimirReporte(self, cr, uid, ids, context=None):
        
        data = {}
        data['model'] = 'remission.guide.ext'
        data['id'] = ids[0]
        data['jasper'] = {
                       'id':ids[0]
                      }
        jasper_reports.report_jasper(
            'report.reportjasper_remission',
            'remission.guide.ext',
            parser=reportjasper_document
        )
        
        return{
               'type':'ir.actions.report.xml',
               'report_name':'reportjasper_remission',
               'datas':data
               }
        
    def unlink(self, cr, uid, ids, context=None):
        if not context:
            context={}
        for guide in self.browse(cr , uid ,ids):
            if guide.state in ('authorized', 'received', 'key_contingency'):
                raise osv.except_osv(_('Error!'), _("No puedes eliminar comprobantes que ya esten autorizados, que esten esperando autorización o que se emitan con claves de contingencia" ))
            docs = self.pool.get('ir.attachment').search(cr, uid ,[('res_id','=',guide.id),('res_model','=','remission.guide.ext')])
            if docs:
                cr.execute('delete from ir_attachment where id IN %s', (tuple(docs),))
        res = super(remission_guide_ext, self).unlink(cr, uid, ids, context)       
        return res 
    
    def action_remission_sent(self, cr, uid, ids, context=None):
        if not context: context= None
        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        ir_model_data = self.pool.get('ir.model.data')
        try:
            template_id = ir_model_data.get_object_reference(cr, uid, 'voucher_electronic', 'email_template_edi_remission_ext')[1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference(cr, uid, 'mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = dict(context)
        ctx.update({
            'default_model': 'remission.guide.ext',
            'default_res_id': ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'mark_invoice_as_sent': True,
            })
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }


class remission_guide_line_ext(osv.osv):
    
    _name = "remission.guide.line.ext"
    _columns={
              'remission_ext_id': fields.many2one('remission.guide.ext', 'Guide Remission', ondelete='cascade', select=True),              
              'codigointerno':fields.char('Cod. Interno', size=64), 
              'descripcion':fields.char('Descripción', size=250), 
              'cantidad': fields.integer('Cant', readonly = True), 
              }

remission_guide_line_ext()
