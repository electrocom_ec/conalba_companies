# -*- encoding: utf-8 -*-
########################################################################
#
# @authors: Jairo Troncoso
# Copyright (C) 2013  CISC
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

{
    "name": "Facturación Electrónica S.R.I",
    "version": "1.0",
    "depends": [ 
                "base",  
                "document",
                "portal",             
                "cisc_base",
                "cisc_states",
                "cisc_verifica_ruc_ci",
                #"report_aeroo",
                #"report_aeroo_ooo",
                ],
                
    "author": "Jairo Troncoso jairom0704@gmail.com",
    "website" : "https://twitter.com/jairomt25",
    "category": "Partners",
    "complexity": "normal",
    "description": """
    La Facturación Electrónica automatiza la emisión de facturas tradicionales, haciendo del proceso más rápido, más barato, con aprobación del SRI, a tan solo un clic de distancia.
    
    La Facturación Electrónica aprobada en línea por el SRI ahorra dinero y permite el intercambio de documentos mercantiles (facturas, certificados de retención en la fuente, notas de débito, notas de crédito y guías de remisión) entre empresas y sus socios de negocios (clientes y/o proveedores).
    """,
    "init_xml": [],
    'update_xml': [
                    'data/send_mail_cron.xml',
                    'data/auth_signup.xml',
                    'views/smsclientele_view.xml',
                    'views/res_company_view.xml',
                   ],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}