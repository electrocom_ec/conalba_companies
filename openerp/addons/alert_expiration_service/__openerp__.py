# -*- encoding: utf-8 -*-
########################################################################
#
# @authors: Jairo Troncoso
# Copyright (C) 2016  CISC
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

{
    "name": "Alerta corte servicio",
    "version": "1.0",
                
    "author": "Jairo Troncoso jairom0704@gmail.com",
    "website" : "https://twitter.com/jairomt25",
    "category": "Partners",
    "complexity": "normal",
    "description": """
    
    """,
    'update_xml': ['views/alert_module_view.xml',
                   'views/res_company_view.xml'],
    'js': ['static/src/js/alert_module.js'],
    'installable': True,
    'auto_install': False,
}
